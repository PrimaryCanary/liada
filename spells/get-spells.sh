#!/usr/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
curl https://5e.tools/data/spells/index.json > index.json
books=()
readarray -t books < index.json
for i in "${books[@]}"
do
    if [ "$i" = "{" ] || [ "$i" = "}" ]
    then
	continue
    else
	file=$(echo "$i" | awk -F ": " '{ gsub ("\"",""); gsub(",",""); print $2 }')
	curl "https://5e.tools/data/spells/$file" > "./$file"
    fi
done
