#!/usr/bin/python

import json, re

WEBSITE = 'https://5e.tools'

def load_spells(sources_file):
    with open(sources_file, "r") as f:
        sources = json.load(f)
    spells = []
    for source in sources.items():
        with open(source[1]) as spell_book:
            json_spells = json.load(spell_book)
            spells.extend(json_spells['spell'])
    return spells

def get_spell(spells, name):
    # Case, whitespace, number, and symbol insensitivity
    name = re.sub(r'[^a-zA-Z]', '', name).lower()
    # Abbreviation
    spells = list(filter(lambda s: s if re.sub(r'[^a-zA-Z]', '', s['name']).lower().startswith(name) else None, spells))
    try:
        # Shortest match
        sorted_spells = sorted(spells, key=lambda k: k['name'])
        return sorted_spells[0]
    except:
        print('Error: Spell not found')
        raise

# Parse tokens embedded in the spells
# Useless ones are discarded, useful ones are replicated in org-mode syntax

# Form: {@dice 2d8+2;3d8 - 1|display text|name of roll}
# Extract dice
def parse_inline_dice(s):
    return re.sub(r'\{@dice ([0-9d +-;]+).*?\}', lambda obj: obj.group(1).replace(' ', ''), s)

# Form: {@chance 33|display text|name of roll}
# Convert to 33 percent
# Only in wish?
def parse_inline_chance(s):
    return re.sub(r'\{@chance ([0-9]+).*?\}', lambda obj: obj.group(1) + ' percent', s)

# Form: {@skill Perception}
# From Jim's Glowing Coin/Jim's Magic Missile/Gift of Gab
# Discarded
def parse_inline_skill(s):
    return re.sub(r'\{@skill ([^\}]*)\}', lambda obj: obj.group(1), s)

# Form: {@hit +7|display text|name of roll}
# Converted to d20+7
def parse_inline_hit(s):
    return re.sub(r'\{@hit ([0-9 +-]+).*?\}', lambda obj: 'd20' + obj.group(1).replace(' ', ''), s)

# Form: {@damage 1d12+3|display text|name of roll}
# Extract dice
def parse_inline_damage(s):
    return re.sub(r'\{@damage ([0-9d +-]+).*?\}', lambda obj: obj.group(1).replace(' ', ''), s)

# Form: {@d20 -4|display text|name of roll}
# Convert to d20-4
def parse_inline_twenty(s):
    return re.sub(r'\{@d20 ([0-9 +-]+).*?\}', lambda obj: 'd20' + obj.group(1).replace(' ', ''), s)

# Form: {@scaledice 1d8|2,4,6,8|1d8}
# Extract dice
def parse_inline_scale_dice(s):
    return re.sub(r'\{@scaledice ([0-9d +-]+).*?\}', lambda obj: obj.group(1).replace(' ', ''), s)

# Form: {@token name or monster|source book|display text}
# except diety which is {@diety name|pantheon|source|display text}
# Last 2 are optional, middle is assumed if not given
# Convert to org-mode link [[website link][name or display text]]
def parse_inline_content_tag(s, token, link, assume, recurse=0):
    def repl(match_obj):
        s = match_obj.group(1).split('|')
        # Parse deities separately because he broke the pattern
        # Asshole
        if token == 'deity' and len(s) > 1 and recurse == 0:
            s[0] += '_' + (assume.split('_')[0] if s[1] == '' else s[1])
            s.remove(s[1])
            return parse_inline_content_tag('{{@deity {}}}'.format('|'.join(s)), token, link, 'phb', 1)
        else:
            # Remove pantheon from name
            clean = lambda s: ''.join(s.split('_')[:-1]) if '_' in s else s
            if len(s) == 1:
                return '[[{}/{}.html#{}_{}][{}]]'.format(WEBSITE, link, s[0].replace(' ', '%20'), assume,  clean(s[0]))
            elif len(s) == 2:
                return '[[{}/{}.html#{}_{}][{}]]'.format(WEBSITE, link, s[0].replace(' ', '%20'), s[1], clean(s[0]))
            elif len(s) == 3 and s[1] == '':
                return '[[{}/{}.html#{}_{}][{}]]'.format(WEBSITE, link, s[0].replace(' ', '%20'), assume, s[2])
            elif len(s) == 3 and s[1] != '':
                return '[[{}/{}.html#{}_{}][{}]]'.format(WEBSITE, link, s[0].replace(' ', '%20'), s[1], s[2])
    return re.sub(r'\{@' + token + ' ([^\}]*)\}', repl, s)

def parse_inline_content(s):
    tags = [
        ['spell', 'spells', 'phb'],
        ['item', 'items', 'dmg'],
        ['creature', 'bestiary', 'mm'],
        ['background', 'backgrounds', 'phb'],
        ['race', 'races', 'phb'],
        ['optfeature', 'optionalfeatures', 'phb'],
        ['class', 'classes', 'phb '],
        ['condition', 'conditionsdiseases', 'phb'],
        ['disease', 'conditionsdiseases', 'dmg'],
        ['reward', 'rewards', 'dmg '],
        ['feat', 'feats', 'phb'],
        ['psionic', 'psionics', 'uathemysticclass'],
        ['object', 'objects', 'dmg'],
        ['boon', 'cultsboons', 'mtf'],
        ['cult', 'cultsboons', 'mtf'],
        ['trap', 'trapshazards', 'dmg'],
        ['hazard', 'trapshazards', 'dmg'],
        ['deity', 'deities', 'forgotten%20realms_phb'],
        ['variantrule', 'variantrules', 'dmg'],
        ['vehicle', 'vehicles', 'gos'],
        ['table', 'tables', 'dmg']
    ]
    for i in tags:
        s = parse_inline_content_tag(s, *i)
    return s

# Fully parsing filter is unlikely, link back to spell
def parse_inline_filter(s, spell):
    def repl(match_obj):
        name = spell['name'].lower().replace(' ', '%20')
        source = spell['source'].lower()
        return '[[{}/spells.html#{}_{}][{}]]'.format(WEBSITE, name, source, match_obj.group(1).split('|')[0])
    return re.subn(r'\{@filter ([^}]*)\}', repl, s, 1)[0]

# quotes, check gift of gab/motivational speech for format
# tables
# lists
# entries?
# bold and italics

def test_inline_parsers():
    spells = load_spells('index.json')
            
    #Basic
    print('dice')
    toll = get_spell(spells, 't0olL*^%^&    th_Edead')
    print(parse_inline_dice(toll['entries'][1]))
    print()

    print('dice')
    print(parse_inline_dice(' {@dice 1d2-2+2d3+5} for regular dice rolls ({@dice 1d6;2d6} for multiple options; {@dice 1d6 + #$prompt_number:min=1,title=Enter a Number!,default=123$#} for input prompts), with extended {@dice 1d20+2|display text} and {@dice 1d20+2|display text|rolled by name}, and a special'))
    print()

    print('chance')
    wish = get_spell(spells, 'Wish')
    print(parse_inline_chance(wish['entries'][4]))
    print()

    print('chance')
    print(parse_inline_chance(' Similar syntax as dice tags: {@chance 50}, {@chance 50|display text}, and {@chance 50|display text|rolled by name}. "Recharge tags; output success/failure for ability'))
    print()

    print('skill')
    jim = get_spell(spells, 'jimsglowingcoin')
    print(parse_inline_skill(jim['entries'][0]))
    print()

    # 3-bar
    print('spell')
    feeb = get_spell(spells, 'Feeblemind')
    print(parse_inline_content_tag(feeb['entries'][3], 'spell', 'spells', 'phb'))
    print()

    print('creature')
    und = get_spell(spells, 'createundead')
    print(parse_inline_content_tag(und['entriesHigherLevel'][0]['entries'][0], 'creature', 'bestiary', 'mm'))
    print()

    print('condition')
    weird = get_spell(spells, 'weird')
    print(parse_inline_content_tag(weird['entries'][0], 'condition', 'conditionsdiseases', 'phb'))
    print()

    print('all inline tags')
    # {@deity shar||scag|other link} has an (expected) broken link
    # There are no forgetton realms deities outside of the phb
    print(parse_inline_content('Spells: {@spell acid splash} assumes PHB by default, {@spell tiny servant|xge} can have sources added with a pipe, {@spell tiny servant|xge|and optional link text added with another pipe}.","Items: {@item alchemy jug} assumed DMG by default, {@item longsword|phb} can have sources added with a pipe, {@item longsword|phb|and optional link text added with another pipe}.","Creatures: {@creature goblin} assumes MM by default, {@creature cow|vgm} can have sources added with a pipe, {@creature cow|vgm|and optional link text added with another pipe}.""Backgrounds: {@background Charlatan} assumes PHB by default, {@background Anthropologist|toa} can have sources added with a pipe, {@background Anthropologist|ToA|and optional link text added with another pipe}.","Races: {@race Human} assumes PHB by default, {@race Aarakocra|eepc} can have sources added with a pipe, {@race Aarakocra|eepc|and optional link text added with another pipe}.","Invocations and Other Optional Features: {@optfeature Agonizing Blast} assumes PHB by default, {@optfeature Aspect of the Moon|xge} can have sources added with a pipe, {@optfeature Aspect of the Moon|xge|and optional link text added with another pipe}.","Classes: {@class fighter} assumes PHB by default, {@class artificer|uaartificer} can have sources added with a pipe, {@class fighter|phb|optional link text added with another pipe}, {@class fighter|phb|subclasses added|Eldritch Knight} with another pipe, {@class fighter|phb|and class feature added|Eldritch Knight|phb|Martial Archetype 3} with another pipe (number denotes level, since names can be duplicate).","Conditions: {@condition stunned} assumes PHB by default, {@condition stunned|PHB} can have sources added with a pipe (not that its ever useful), {@condition stunned|PHB|and optional link text added with another pipe}.","Diseases: {@disease cackle fever} assumes DMG by default, {@disease cackle fever} can have sources added with a pipe, {@disease cackle fever|DMG|and optional link text added with another pipe}.","Other Rewards: {@reward Blessing of Health} assumes DMG by default, {@reward Blessing of Health} can have sources added with a pipe, {@reward Blessing of Health|DMG|and optional link text added with another pipe}.","Feats: {@feat Alert} assumes PHB by default, {@feat Elven Accuracy|xge} can have sources added with a pipe, {@feat Elven Accuracy|xge|and optional link text added with another pipe}.","Psionics: {@psionic Mastery of Force} assumes UATheMysticClass by default, {@psionic Mastery of Force|UATheMysticClass} can have sources added with a pipe, {@psionic Mastery of Force|UATheMysticClass|and optional link text added with another pipe}.","Objects: {@object Ballista} assumes DMG by default, {@object Ballista} can have sources added with a pipe, {@object Ballista|DMG|and optional link text added with another pipe}.","Boons: {@boon Demogorgon} assumes MTF by default, {@boon Demogorgon} can have sources added with a pipe, {@boon Demogorgon|MTF|and optional link text added with another} "Cults: {@cult Cult of Asmodeus} assumes MTF by default, {@cult Cult of Asmodeus} can have sources added with a pipe, {@cult Cult of Asmodeus|MTF|and optional link text added with another pipe}.","Traps: {@trap falling net} assumes DMG by default, {@trap falling portcullis|xge} can have sources added with a pipe, {@trap falling portcullis|xge|and optional link text added with another pipe}.",Hazards: {@hazard brown mold} assumes DMG by default, {@hazard russet mold|vgm} can have sources added with a pipe, {@hazard russet mold|vgm|and optional link text added with another pipe}.","Deities: {@deity Gond} assumes PHB Forgotten Realms pantheon by default, {@deity Gruumsh|nonhuman} can have pantheons added with a pipe, {@deity Ioun|dawn war|dmg} can have sources added with another pipe, {@deity Ioun|dawn war|dmg|and optionallink text added with another____pipe}. alsdjflkak {@deity shar||scag|other link} lasjdlfkjlskjdlfajksdjflasdfas {@deity Lugh|celtic||tough link} df asdf asdfasdfajlsdjflajs a  ljd {@deity Gond|||link}",","Variant rules: {@variantrule Diagonals} assumes DMG by default, {@variantrule Multiclassing|phb} can have sources added with a pipe, {@variantrule Multiclassing|phb|and optional link text added with another pipe}.","Vehicles (formerly Ships): {@vehicle Galley} assumes GoS by default, {@vehicle Galley|UAOfShipsAndSea} can have sources added with a pipe, {@vehicle Galley|GoS|and optional link text added with another pipe}.",Tables: {@table 25 gp Art Objects} assumes DMG by default, {@table Adventuring Gear|phb} can have sources added with a pipe, {@table Adventuring Gear|phb|and optional link text added with another pipe}."'))
    #print()
    
    # Complex
    print('filter')
    seq = get_spell(spells, 'sequest')
    print(parse_inline_filter(seq['entries'][0], seq))
    print()
    
def parse_school(spell):
    schools = {
        'A': 'Abjuration',
        'C': 'Conjuration',
        'D': 'Divination',
        'E': 'Enchantment',
        'I': 'Illusion',
        'N': 'Necromancy',
        'P': 'Psionic',
        'T': 'Transmutation',
        'V': 'Evocation'
    }
    return schools[spell['school']]

def parse_meta(spell):
    if 'meta' in spell.keys():
        return ' '.join(map(lambda k: '({})'.format(k.lower()), spell['meta'].keys()))
    else:
        return ''
    
def parse_level_school_meta(spell):
    l = spell['level']
    school = parse_school(spell)
    meta = parse_meta(spell)
    th = [4, 5, 6, 8]
    if l == 0:
        return '{} cantrip {}'.format(school, meta)
    elif l == 1:
        return '1st-level {} {}'.format(school, meta)
    elif l == 2:
        return '2nd-level {} {}'.format(school, meta)
    elif l == 3:
        return '3rd-level {} {}'.format(school, meta)
    else:
        return '{}th-level {} {}'.format(l, school, meta)

def parse_cast_time(spell):
    def combine(time):
        s = str(time['number']) + ' '
        s += time['unit'] + 's' if time['number'] > 1 else time['unit']
        cond = time.get('condition')
        s += ', ' + cond if cond else ''
        return s
    return ' or '.join(map(combine, spell['time']))

def parse_range(spell):
    RNG_SPECIAL = "special"
    RNG_POINT = 'point'
    RNG_LINE = 'line'
    RNG_CUBE = 'cube'
    RNG_CONE = 'cone'
    RNG_RADIUS = 'radius'
    RNG_SPHERE = 'sphere'
    RNG_HEMISPHERE = 'hemisphere'
    RNG_CYLINDER = 'cylinder' #homebrew only
    RNG_SELF = 'self'
    RNG_SIGHT = 'sight'
    RNG_UNLIMITED = 'unlimited'
    RNG_UNLIMITED_SAME_PLANE = 'plane'
    RNG_TOUCH = 'touch'
    range_types = {
        RNG_SPECIAL: 'Special',
	RNG_POINT: 'Point',
	RNG_LINE: 'Line',
	RNG_CUBE: 'Cube',
	RNG_CONE: 'Cone',
	RNG_RADIUS: 'Radius',
	RNG_SPHERE: 'Sphere',
	RNG_HEMISPHERE: 'Hemisphere',
	RNG_CYLINDER: 'Cylinder',
	RNG_SELF: 'Self',
	RNG_SIGHT: 'Sight',
	RNG_UNLIMITED: 'Unlimited',
	RNG_UNLIMITED_SAME_PLANE: 'Unlimited on the same plane',
	RNG_TOUCH: 'Touch'
    }
    UNT_FEET = 'feet'
    UNT_MILES = 'miles'
    dist_types = {
	UNT_FEET: 'Feet',
	UNT_MILES: 'Miles',
	RNG_SELF: range_types[RNG_SELF],
	RNG_TOUCH: range_types[RNG_TOUCH],
	RNG_SIGHT: range_types[RNG_SIGHT],
	RNG_UNLIMITED: range_types[RNG_UNLIMITED],
	RNG_UNLIMITED_SAME_PLANE: range_types[RNG_UNLIMITED_SAME_PLANE]
    }

    range = spell['range']
    
    def get_singleton_unit(unit):
        if unit == UNT_FEET:
            return 'foot'
        elif unit == UNT_MILES:
            return 'mile'
        else:
            if unit[-1] == 's':
                return unit[:-1]
            else:
                return unit
    
    def render_point():
        dist = range['distance']
        render_p = [RNG_SPECIAL, UNT_FEET, UNT_MILES]
        if dist['type'] == RNG_SELF:
            return range_types[dist['type']]
        elif dist['type'] == RNG_SIGHT:
            return range_types[dist['type']]
        elif dist['type'] == RNG_UNLIMITED:
            return range_types[dist['type']]
        elif dist['type'] == RNG_UNLIMITED_SAME_PLANE:
            return range_types[dist['type']]
        elif dist['type'] == RNG_TOUCH:
            return range_types[dist['type']]
        elif dist['type'] in render_p:
            return '{} {}'.format(dist['amount'], get_singleton_unit(dist['type']) if dist['amount'] == 1 else dist['type'])
        else:
            return '{} {}'.format(dist['amount'], get_singleton_unit(dist['type']) if dist['amount'] == 1 else dist['type'])

    def render_area():
        def get_area_style():
            if range['type'] == RNG_SPHERE:
                return ' radius'
            elif range['type'] == RNG_HEMISPHERE:
                return '-radius ' + range['type']
            elif range['type'] == RNG_CYLINDER:
                return "-radius"
            else:
                return ' ' + range['type']
            
        size = range['distance']
        label = ', {}-{}-high cylinder'.format(size['amountSecondary'], get_singleton_unit(size['typeSecondary'])) if range['type'] == RNG_CYLINDER else ''
        return 'Self ({}-{}{}{})'.format(size['amount'], get_singleton_unit(size['type']), get_area_style(), label)

    render_a = [RNG_LINE, RNG_CUBE, RNG_CONE, RNG_RADIUS, RNG_SPHERE, RNG_HEMISPHERE, RNG_CYLINDER]
    if range['type'] == RNG_SPECIAL:
        return range_types[range['type']]
    elif range['type'] == RNG_POINT:
        return render_point()
    elif range['type'] in render_a:
        return render_area()

def parse_components(spell):
    comp = spell.get('components')
    if comp:
        out = []
        out.append('V') if 'v' in comp else ''
        out.append('S') if 's' in comp else ''
        if 'm' in comp:
            s = 'M'
            if type(comp['m']) == type(dict()):
                t = comp['m'].get('text')
                s += ' ({})'.format(t) if t else ''
            else:
                s += ' ({})'.format(comp['m'])
            out.append(s)
        out.append('R ({} gp)'.format(spell['level'])) if 'r' in comp else ''
        return ', '.join(out)
    else:
        return 'None'

def parse_duration(spell):
    dur = spell['duration']
    end_types = {
        'dispel': 'dispelled',
        'trigger': 'triggered',
        'discharge': 'discharged'
    }

    def dur_to_text(d):
        if d['type'] == 'special':
            return 'Special'
        elif d['type'] == 'instant':
            cond = d.get('condition')
            return 'Instantaneous' + (' ({})'.format(cond) if cond else '')
        elif d['type'] == 'timed':
            s = ''
            conc = d.get('concentration')
            upto = d['duration'].get('upTo')
            amount = d['duration']['amount']
            dtype = d['duration']['type']
            s += 'Concentration, ' if conc else ''
            s += 'u' if conc else 'U' if upto  else ''
            s += 'p to ' if conc or upto  else ''
            s += '{} {}'.format(amount, dtype) + ('s' if amount > 1 else '')
            return s
        elif d['type'] == 'permanent':
            ends = d.get('ends')
            if ends:
                return 'Until ' + ' or '.join(map(lambda m: end_types[m], ends))
            else:
                return 'Permanent'
    return ' or '.join(map(dur_to_text, dur)) + (' (see below)' if len(dur) > 1 else '')

def main():
    spells = load_spells('index.json')
    name = 'contag'
    spell = get_spell(spells, name)
    get_spell(spells, name)
    print(spell['name'])
    print(parse_level_school_meta(spell))
    print(parse_cast_time(spell))
    #print(parse_range(spell))
    print(parse_components(spell))
    print(parse_duration(spell))

    print(spell['entries'])
    # Tests for parsing entries
    # alter self, contagion, animate objects, antimagic field, augury, chaos bolt, confusion, control weather, druid grove (for the end), imprisonment, motivational speech, reincarnate, scrying, teleport
    #print(len(spell['entries']))
    #print(spell['entriesHigherLevel'])

if __name__ == '__main__':
    main()
