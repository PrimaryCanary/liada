import unittest as ut
import parse as p

class TestLoad(ut.TestCase):
    #TODO: test load_spells
    def setUp(self):
        self.spells = p.load_spells('index.json')
        
    def test_get_spells_first_match(self):
        s = p.get_spell(self.spells, 'Toll the Dead')
        self.assertEqual(s['name'], 'Toll the Dead', 'First match')

    def test_get_spells_exact_match(self):
        s = p.get_spell(self.spells, 'Toll the Dead (UA)')
        self.assertEqual(s['name'], 'Toll the Dead (UA)', 'Exact match')

    def test_get_spells_abbreviated(self):
        s = p.get_spell(self.spells, 'Toll the')
        self.assertEqual(s['name'], 'Toll the Dead', 'Abbreviated')

    def test_get_spells_insensitive(self):
        s = p.get_spell(self.spells, 't*&^o567L  ?" l tH12e9de-ad')
        self.assertEqual(s['name'], 'Toll the Dead', 'Case, number, etc. insensitive')

    def test_get_spells_not_found(self):
        with self.assertRaises(Exception):
            s = p.get_spell(self.spells, 'non-existent spell')

class TestParseInlineRoller(ut.TestCase):
    def test_dice(self):
        s = '{@dice 1d2-2+2 d3+ 5} regular ({@dice 1d6;2d6}) options {@dice 1d6 + #$prompt_number:min=1,title=Enter a Number!,default=123$#} input prompts {@dice 1d20+2|display text} display text {@dice 1d20+2|display text|rolled by name} named roll'
        exp = '1d2-2+2d3+5 regular (1d6;2d6) options 1d6+ input prompts 1d20+2 display text 1d20+2 named roll'
        self.assertEqual(p.parse_inline_dice(s), exp, 'Dice')

    def test_chance(self):
        s = '{@chance 50} normal {@chance 50|display text} display {@chance 50|display text|rolled by name} named'
        exp = '50 percent normal 50 percent display 50 percent named'
        self.assertEqual(p.parse_inline_chance(s), exp, 'Chance')

    def test_skill(self):
        s = '{@skill Perception} normal'
        exp = 'Perception normal'
        self.assertEqual(p.parse_inline_skill(s), exp, 'Skill')

    def test_hit(self):
        s = '{@hit +7 -2|display|name} normal'
        exp = 'd20+7-2 normal'
        self.assertEqual(p.parse_inline_hit(s), exp, 'Hit')

    def test_damage(self):
        s = '{@damage 1d12-1 +3|display|name} normal'
        exp = '1d12-1+3 normal'
        self.assertEqual(p.parse_inline_damage(s), exp, 'Damage')

    def test_twenty(self):
        s = '{@d20 -4 + 2|display|name} normal'
        exp = 'd20-4+2 normal'
        self.assertEqual(p.parse_inline_twenty(s), exp, 'd20')

    def test_scale_dice(self):
        s = '{@scaledice 1+1d 8- 2|2,4,6,8|1d8} normal'
        exp = '1+1d8-2 normal'
        self.assertEqual(p.parse_inline_scale_dice(s), exp, 'Scaling dice')

class TestParseInlineContent
    #TODO: Break it up this monolith
    def test_tags(self):
        # {@deity shar||scag|other link} has an (expected) broken link
        # There are no forgetton realms deities outside of the phb
        s = 'Spells: {@spell acid splash} assumes PHB by default, {@spell tiny servant|xge} can have sources added with a pipe, {@spell tiny servant|xge|and optional link text added with another pipe}.","Items: {@item alchemy jug} assumed DMG by default, {@item longsword|phb} can have sources added with a pipe, {@item longsword|phb|and optional link text added with another pipe}.","Creatures: {@creature goblin} assumes MM by default, {@creature cow|vgm} can have sources added with a pipe, {@creature cow|vgm|and optional link text added with another pipe}.""Backgrounds: {@background Charlatan} assumes PHB by default, {@background Anthropologist|toa} can have sources added with a pipe, {@background Anthropologist|ToA|and optional link text added with another pipe}.","Races: {@race Human} assumes PHB by default, {@race Aarakocra|eepc} can have sources added with a pipe, {@race Aarakocra|eepc|and optional link text added with another pipe}.","Invocations and Other Optional Features: {@optfeature Agonizing Blast} assumes PHB by default, {@optfeature Aspect of the Moon|xge} can have sources added with a pipe, {@optfeature Aspect of the Moon|xge|and optional link text added with another pipe}.","Classes: {@class fighter} assumes PHB by default, {@class artificer|uaartificer} can have sources added with a pipe, {@class fighter|phb|optional link text added with another pipe}, {@class fighter|phb|subclasses added|Eldritch Knight} with another pipe, {@class fighter|phb|and class feature added|Eldritch Knight|phb|Martial Archetype 3} with another pipe (number denotes level, since names can be duplicate).","Conditions: {@condition stunned} assumes PHB by default, {@condition stunned|PHB} can have sources added with a pipe (not that its ever useful), {@condition stunned|PHB|and optional link text added with another pipe}.","Diseases: {@disease cackle fever} assumes DMG by default, {@disease cackle fever} can have sources added with a pipe, {@disease cackle fever|DMG|and optional link text added with another pipe}.","Other Rewards: {@reward Blessing of Health} assumes DMG by default, {@reward Blessing of Health} can have sources added with a pipe, {@reward Blessing of Health|DMG|and optional link text added with another pipe}.","Feats: {@feat Alert} assumes PHB by default, {@feat Elven Accuracy|xge} can have sources added with a pipe, {@feat Elven Accuracy|xge|and optional link text added with another pipe}.","Psionics: {@psionic Mastery of Force} assumes UATheMysticClass by default, {@psionic Mastery of Force|UATheMysticClass} can have sources added with a pipe, {@psionic Mastery of Force|UATheMysticClass|and optional link text added with another pipe}.","Objects: {@object Ballista} assumes DMG by default, {@object Ballista} can have sources added with a pipe, {@object Ballista|DMG|and optional link text added with another pipe}.","Boons: {@boon Demogorgon} assumes MTF by default, {@boon Demogorgon} can have sources added with a pipe, {@boon Demogorgon|MTF|and optional link text added with another} "Cults: {@cult Cult of Asmodeus} assumes MTF by default, {@cult Cult of Asmodeus} can have sources added with a pipe, {@cult Cult of Asmodeus|MTF|and optional link text added with another pipe}.","Traps: {@trap falling net} assumes DMG by default, {@trap falling portcullis|xge} can have sources added with a pipe, {@trap falling portcullis|xge|and optional link text added with another pipe}.",Hazards: {@hazard brown mold} assumes DMG by default, {@hazard russet mold|vgm} can have sources added with a pipe, {@hazard russet mold|vgm|and optional link text added with another pipe}.","Deities: {@deity Gond} assumes PHB Forgotten Realms pantheon by default, {@deity Gruumsh|nonhuman} can have pantheons added with a pipe, {@deity Ioun|dawn war|dmg} can have sources added with another pipe, {@deity Ioun|dawn war|dmg|and optionallink text added with another____pipe}. alsdjflkak {@deity shar||scag|other link} lasjdlfkjlskjdlfajksdjflasdfas {@deity Lugh|celtic||tough link} df asdf asdfasdfajlsdjflajs a  ljd {@deity Gond|||link}",","Variant rules: {@variantrule Diagonals} assumes DMG by default, {@variantrule Multiclassing|phb} can have sources added with a pipe, {@variantrule Multiclassing|phb|and optional link text added with another pipe}.","Vehicles (formerly Ships): {@vehicle Galley} assumes GoS by default, {@vehicle Galley|UAOfShipsAndSea} can have sources added with a pipe, {@vehicle Galley|GoS|and optional link text added with another pipe}.",Tables: {@table 25 gp Art Objects} assumes DMG by default, {@table Adventuring Gear|phb} can have sources added with a pipe, {@table Adventuring Gear|phb|and optional link text added with another pipe}."'
        exp = 'Spells: [[https://5e.tools/spells.html#acid%20splash_phb][acid splash]] assumes PHB by default, [[https://5e.tools/spells.html#tiny%20servant_xge][tiny servant]] can have sources added with a pipe, [[https://5e.tools/spells.html#tiny%20servant_xge][and optional link text added with another pipe]].","Items: [[https://5e.tools/items.html#alchemy%20jug_dmg][alchemy jug]] assumed DMG by default, [[https://5e.tools/items.html#longsword_phb][longsword]] can have sources added with a pipe, [[https://5e.tools/items.html#longsword_phb][and optional link text added with another pipe]].","Creatures: [[https://5e.tools/bestiary.html#goblin_mm][goblin]] assumes MM by default, [[https://5e.tools/bestiary.html#cow_vgm][cow]] can have sources added with a pipe, [[https://5e.tools/bestiary.html#cow_vgm][and optional link text added with another pipe]].""Backgrounds: [[https://5e.tools/backgrounds.html#Charlatan_phb][Charlatan]] assumes PHB by default, [[https://5e.tools/backgrounds.html#Anthropologist_toa][Anthropologist]] can have sources added with a pipe, [[https://5e.tools/backgrounds.html#Anthropologist_ToA][and optional link text added with another pipe]].","Races: [[https://5e.tools/races.html#Human_phb][Human]] assumes PHB by default, [[https://5e.tools/races.html#Aarakocra_eepc][Aarakocra]] can have sources added with a pipe, [[https://5e.tools/races.html#Aarakocra_eepc][and optional link text added with another pipe]].","Invocations and Other Optional Features: [[https://5e.tools/optionalfeatures.html#Agonizing%20Blast_phb][Agonizing Blast]] assumes PHB by default, [[https://5e.tools/optionalfeatures.html#Aspect%20of%20the%20Moon_xge][Aspect of the Moon]] can have sources added with a pipe, [[https://5e.tools/optionalfeatures.html#Aspect%20of%20the%20Moon_xge][and optional link text added with another pipe]].","Classes: [[https://5e.tools/classes.html#fighter_phb ][fighter]] assumes PHB by default, [[https://5e.tools/classes.html#artificer_uaartificer][artificer]] can have sources added with a pipe, [[https://5e.tools/classes.html#fighter_phb][optional link text added with another pipe]],  with another pipe,  with another pipe (number denotes level, since names can be duplicate).","Conditions: [[https://5e.tools/conditionsdiseases.html#stunned_phb][stunned]] assumes PHB by default, [[https://5e.tools/conditionsdiseases.html#stunned_PHB][stunned]] can have sources added with a pipe (not that its ever useful), [[https://5e.tools/conditionsdiseases.html#stunned_PHB][and optional link text added with another pipe]].","Diseases: [[https://5e.tools/conditionsdiseases.html#cackle%20fever_dmg][cackle fever]] assumes DMG by default, [[https://5e.tools/conditionsdiseases.html#cackle%20fever_dmg][cackle fever]] can have sources added with a pipe, [[https://5e.tools/conditionsdiseases.html#cackle%20fever_DMG][and optional link text added with another pipe]].","Other Rewards: [[https://5e.tools/rewards.html#Blessing%20of%20Health_dmg ][Blessing of Health]] assumes DMG by default, [[https://5e.tools/rewards.html#Blessing%20of%20Health_dmg ][Blessing of Health]] can have sources added with a pipe, [[https://5e.tools/rewards.html#Blessing%20of%20Health_DMG][and optional link text added with another pipe]].","Feats: [[https://5e.tools/feats.html#Alert_phb][Alert]] assumes PHB by default, [[https://5e.tools/feats.html#Elven%20Accuracy_xge][Elven Accuracy]] can have sources added with a pipe, [[https://5e.tools/feats.html#Elven%20Accuracy_xge][and optional link text added with another pipe]].","Psionics: [[https://5e.tools/psionics.html#Mastery%20of%20Force_uathemysticclass][Mastery of Force]] assumes UATheMysticClass by default, [[https://5e.tools/psionics.html#Mastery%20of%20Force_UATheMysticClass][Mastery of Force]] can have sources added with a pipe, [[https://5e.tools/psionics.html#Mastery%20of%20Force_UATheMysticClass][and optional link text added with another pipe]].","Objects: [[https://5e.tools/objects.html#Ballista_dmg][Ballista]] assumes DMG by default, [[https://5e.tools/objects.html#Ballista_dmg][Ballista]] can have sources added with a pipe, [[https://5e.tools/objects.html#Ballista_DMG][and optional link text added with another pipe]].","Boons: [[https://5e.tools/cultsboons.html#Demogorgon_mtf][Demogorgon]] assumes MTF by default, [[https://5e.tools/cultsboons.html#Demogorgon_mtf][Demogorgon]] can have sources added with a pipe, [[https://5e.tools/cultsboons.html#Demogorgon_MTF][and optional link text added with another]] "Cults: [[https://5e.tools/cultsboons.html#Cult%20of%20Asmodeus_mtf][Cult of Asmodeus]] assumes MTF by default, [[https://5e.tools/cultsboons.html#Cult%20of%20Asmodeus_mtf][Cult of Asmodeus]] can have sources added with a pipe, [[https://5e.tools/cultsboons.html#Cult%20of%20Asmodeus_MTF][and optional link text added with another pipe]].","Traps: [[https://5e.tools/trapshazards.html#falling%20net_dmg][falling net]] assumes DMG by default, [[https://5e.tools/trapshazards.html#falling%20portcullis_xge][falling portcullis]] can have sources added with a pipe, [[https://5e.tools/trapshazards.html#falling%20portcullis_xge][and optional link text added with another pipe]].",Hazards: [[https://5e.tools/trapshazards.html#brown%20mold_dmg][brown mold]] assumes DMG by default, [[https://5e.tools/trapshazards.html#russet%20mold_vgm][russet mold]] can have sources added with a pipe, [[https://5e.tools/trapshazards.html#russet%20mold_vgm][and optional link text added with another pipe]].","Deities: [[https://5e.tools/deities.html#Gond_forgotten%20realms_phb][Gond]] assumes PHB Forgotten Realms pantheon by default, [[https://5e.tools/deities.html#Gruumsh_nonhuman_phb][Gruumsh]] can have pantheons added with a pipe, [[https://5e.tools/deities.html#Ioun_dawn%20war_dmg][Ioun]] can have sources added with another pipe, [[https://5e.tools/deities.html#Ioun_dawn%20war_dmg][and optionallink text added with another____pipe]]. alsdjflkak [[https://5e.tools/deities.html#shar_forgotten%20realms_scag][other link]] lasjdlfkjlskjdlfajksdjflasdfas [[https://5e.tools/deities.html#Lugh_celtic_phb][tough link]] df asdf asdfasdfajlsdjflajs a  ljd [[https://5e.tools/deities.html#Gond_forgotten%20realms_phb][link]]",","Variant rules: [[https://5e.tools/variantrules.html#Diagonals_dmg][Diagonals]] assumes DMG by default, [[https://5e.tools/variantrules.html#Multiclassing_phb][Multiclassing]] can have sources added with a pipe, [[https://5e.tools/variantrules.html#Multiclassing_phb][and optional link text added with another pipe]].","Vehicles (formerly Ships): [[https://5e.tools/vehicles.html#Galley_gos][Galley]] assumes GoS by default, [[https://5e.tools/vehicles.html#Galley_UAOfShipsAndSea][Galley]] can have sources added with a pipe, [[https://5e.tools/vehicles.html#Galley_GoS][and optional link text added with another pipe]].",Tables: [[https://5e.tools/tables.html#25%20gp%20Art%20Objects_dmg][25 gp Art Objects]] assumes DMG by default, [[https://5e.tools/tables.html#Adventuring%20Gear_phb][Adventuring Gear]] can have sources added with a pipe, [[https://5e.tools/tables.html#Adventuring%20Gear_phb][and optional link text added with another pipe]]."'
        self.assertEqual(p.parse_inline_content(s), exp, 'Monolith tags')

def suite():
    suite = ut.TestSuite()
    loader = ut.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(TestLoad))
    suite.addTests(loader.loadTestsFromTestCase(TestParseInline))
    return suite

if __name__ == '__main__':
    runner = ut.TextTestRunner()
    runner.run(suite())
